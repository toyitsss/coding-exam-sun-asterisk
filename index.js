const express = require("express");
const mongoose = require("mongoose");
// const cors = require("cors");
const articleRoutes = require("./routes/articleRoutes")
const app = express();
const router = express.Router();


mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.2q2jw.mongodb.net/exam?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true
	})

mongoose.connection.once('open', () => console.log ('Connected to Database'))



app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.use("/articles", articleRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});