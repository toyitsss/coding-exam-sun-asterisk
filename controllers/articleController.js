const Article = require('../models/Article');





module.exports.getAll = () => {
	return Article.find().then(result => {
		return result;
	})
}

module.exports.addArticle = (body) => {
	let newArticle = new Article ({
		title: body.name,
		content: body.content
	});
	return newArticle.save().then((article, err) => {
		if (err) {
			return false;
		}else{
			return true;
		}
	})


}


module.exports.updateArticle = (articleId, body) => {
	let updatedArticle = {
		title : body.title,
		content	: body.content
	}

	return Article.findByIdAndUpdate(articleId, updatedArticle).then((article, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}


module.exports.delete = (articleId) => {
	return Article.findByIdAndDelete(articleId).then(result => {
		if (result){
		return true
		}
	})
}




module.exports.getArticleById = (articleId) => {
	return Article.findById(articleId).then((result, error) => {
		if(error){
			console.log(error)
			return false;
		}else{
			return result;
		}
	})
}









