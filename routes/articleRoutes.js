const express = require('express');
const router = express.Router();
const articleController = require('../controllers/articleController')


router.get("/all", (req, res) => {
	articleController.getAll().then(resultFromController => res.send(resultFromController));
})



router.post('/', (req, res) => {
	articleController.addArticle(req.body).then(result => res.send(result));
})


router.put('/:articleId', (req, res) => {
	articleController.updateArticle(req.params.articleId, req.body)
   .then(result => res.send(result))
})



router.delete('/:articleId/delete', (req, res) => {
	articleController.delete(req.params.articleId).then(result => res.send(result))
})


router.get('/:articleId', (req, res) => {
	let paramsId = req.params.articleId
	articleController.getArticleById(paramsId).then(result => res.send(result))
})




module.exports = router;